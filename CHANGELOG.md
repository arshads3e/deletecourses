# Changelog

## 3.4.1 (June 22, 2018)

- Implement specific permission for tool

## 3.4.0 (May 13, 2018)

- Updated for GDPR compliance
- Changed version numbering to match stable version

## 1.0.0 (May 12, 2017)

- Initial release